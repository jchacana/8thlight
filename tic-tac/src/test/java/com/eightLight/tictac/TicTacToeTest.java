package com.eightLight.tictac;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class TicTacToeTest {
	
	char [][] boardArray = {{' ', ' ', ' '},{' ', ' ', ' '},{' ', ' ', ' '}};
	Board board = new Board(boardArray);
	
	@Test
	public void shouldBeEqualArray(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'X', 'X'};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', ' '};
		board.setBoard(boardArray);
		
		Board toBeTested = new Board(board);
		assertTrue(Arrays.deepEquals(board.getBoard(), toBeTested.getBoard()));
	}

	@Test
	public void shouldMoveX() throws TicTacToeException{
		int x,y;
		x = y = 0;
		Player player = new Player(board);
		player.setSymbol(Symbol.X);
		assertEquals(Symbol.X.getSymbol(), player.move(x, y));
	}
	
	@Test
	public void shouldMoveO() throws TicTacToeException{
		int x,y;
		x = y = 1;
		Player player = new Player(board);
		player.setSymbol(Symbol.O);
		assertEquals(Symbol.O.getSymbol(), player.move(x, y));
	}
	
	@Test(expected = TicTacToeException.class)
	public void shouldBeOffLimits() throws TicTacToeException{
		Player player = new Player(board);
		player.move(3, 3);
	}
	
	@Test(expected = TicTacToeException.class)
	public void shouldBeOccupied() throws TicTacToeException {
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'X', 'X'};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		player.move(0, 0);
	}
	
	@Test
	public void shouldWinByHorizontal(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'X', 'X'};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkHorizontal());
	}
	
	@Test
	public void shouldWinByVertical(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{'X', ' ', ' '};
		boardArray[2] = new char[]{'X', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkVertical());
	}
	
	@Test
	public void shouldWinByFirstColumn(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{'X', ' ', ' '};
		boardArray[2] = new char[]{'X', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkColumn(0));
	}
	
	@Test
	public void shouldWinBySecondColumn(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{' ', 'X', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', 'X', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkColumn(1));
	}
	
	@Test
	public void shouldWinByFirstRow(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'X', 'X'};
		boardArray[1] = new char[]{' ', ' ', ' '};
		boardArray[2] = new char[]{' ', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkRow(0));
	}
	
	
	@Test
	public void shouldWinByDiagonalPrimary(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', 'X'};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkDiagonals());
	}
	
	@Test
	public void shouldWinByDiagonalSecondary(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{' ', ' ', 'X'};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{'X', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkDiagonals());
	}
	
	@Test
	public void shouldWinByAny(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{'X', 'X', ' '};
		boardArray[2] = new char[]{'X', ' ', ' '};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.winCondition());
	}
	
	@Test
	public void shouldDraw(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{1, 0, 0};
		boardArray[1] = new char[]{0, 1, 1};
		boardArray[2] = new char[]{1, 0, 0};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.checkDraw());
	}
	
	@Test
	public void shouldWinX(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', 'X'};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.winCondition());
	}
	
	@Test
	public void shouldLoseX(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'O', ' ', ' '};
		boardArray[1] = new char[]{' ', 'O', ' '};
		boardArray[2] = new char[]{' ', ' ', 'O'};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.X);
		assertTrue(player.loseCondition());
	}
	
	@Test
	public void shouldWinO(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'O', ' ', ' '};
		boardArray[1] = new char[]{' ', 'O', ' '};
		boardArray[2] = new char[]{' ', ' ', 'O'};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.O);
		assertTrue(player.winCondition());
	}
	
	@Test
	public void shouldLoseO(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', 'X'};
		board.setBoard(boardArray);
		Player player = new Player(board, Symbol.O);
		assertTrue(player.loseCondition());
	}
	
	@Test
	public void shouldEndGameOnWin(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', 'X'};
		board.setBoard(boardArray);
		assertTrue(board.endState());
	}
	
	@Test
	public void shouldEndGameOnTie(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'O', 'X'};
		boardArray[1] = new char[]{'O', 'X', 'X'};
		boardArray[2] = new char[]{'O', 'X', 'O'};
		board.setBoard(boardArray);
		assertTrue(board.endState());
	}
	
	@Test
	public void shouldNotEndGame(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', ' ', ' '};
		boardArray[2] = new char[]{' ', ' ', ' '};
		board.setBoard(boardArray);
		assertTrue(!board.endState());
	}
	
	@Test
	public void shouldBeFullBoard(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'O', 'X'};
		boardArray[1] = new char[]{'O', 'X', 'X'};
		boardArray[2] = new char[]{'O', 'X', 'O'};
		board.setBoard(boardArray);
		assertTrue(board.isFullBoard());
	}
	
	@Test
	public void shouldNotBeFullBoard(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', ' ', ' '};
		boardArray[2] = new char[]{' ', ' ', ' '};
		board.setBoard(boardArray);
		assertTrue(!board.isFullBoard());
	}
	
	@Test
	public void verifySetup(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', 'X'};
		board.setBoard(boardArray);
		Player player1 = new Player(board, Symbol.X);
		Player player2 = new Player(board, Symbol.O);
		TicTacToe game = new TicTacToe(board, player1, player2);
		assertEquals(board, game.getBoard());
		assertEquals(board, game.getPlayer1().getBoard());
	}
	
	@Test
	public void testPrint(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', 'O', 'O'};
		boardArray[1] = new char[]{'O', 'X', 'O'};
		boardArray[2] = new char[]{'O', 'O', 'X'};
		board.setBoard(boardArray);
		board.printCurrentBoard();
	}
	
	public void testMenu(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'X', ' ', ' '};
		boardArray[1] = new char[]{' ', 'X', ' '};
		boardArray[2] = new char[]{' ', ' ', 'X'};
		board.setBoard(boardArray);
		Player player1 = new Player(board, Symbol.X);
		Player player2 = new Player(board, Symbol.O);
		TicTacToe game = new TicTacToe(board, player1, player2);
		game.printMenu();
	}
	
	@Test
	public void listOfLevelPossibleMovesTest(){
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'O', ' ', 'X'};
		boardArray[1] = new char[]{'X', ' ', ' '};
		boardArray[2] = new char[]{'X', 'O', 'O'};
		board.setBoard(boardArray);
		Player player = new PerfectPlayer(board, Symbol.X);
		List<int[]> expected = new ArrayList<int[]>();
		expected.add(new int[]{0,1});
		expected.add(new int[]{1,1});
		expected.add(new int[]{1,2});
		
		Player player1 = new PerfectPlayer(board, Symbol.X);
		Player player2 = new PerfectPlayer(board, Symbol.O);
		TicTacToe game = new PerfectTicTacToe(board, player1, player2);
		
		List<int[]> moves = ((PerfectTicTacToe)game).generateCurrentLevelMoves(board);
		assertTrue(Arrays.deepEquals(expected.toArray(), moves.toArray()));
	}
	
	@Test
	/**
	 * According to http://neverstopbuilding.com/minimax
	 * the next best move for player X is 1,1 which ends the game
	 */
	public void shouldGiveBestMove() throws TicTacToeException{
		boardArray = new char[3][3];
		boardArray[0] = new char[]{'O', ' ', 'X'};
		boardArray[1] = new char[]{'X', ' ', ' '};
		boardArray[2] = new char[]{'X', 'O', 'O'};
		board.setBoard(boardArray);
		Player player1 = new PerfectPlayer(board, Symbol.X);
		Player player2 = new Player(board, Symbol.O);
		TicTacToe game = new PerfectTicTacToe(board, player1, player2);
		
		int [] expected = new int[]{1, 1};
		game.setActiveTurn(player1);
		game.play(player1);
		int [] move = ((PerfectTicTacToe)game).getNextBestPossibleMove();
		assertArrayEquals(expected, move);
	}
	
	
}
