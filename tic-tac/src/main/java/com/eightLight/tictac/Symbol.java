package com.eightLight.tictac;

public enum Symbol {
	X('X'),
	O('O'),
	VOID(' ');
	
	private char symbol;
	Symbol(char symbol){
		this.symbol = symbol;
	}
	
	public char getSymbol() {
		return symbol;
	}
	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}
}