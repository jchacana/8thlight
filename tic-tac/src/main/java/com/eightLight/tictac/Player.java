package com.eightLight.tictac;

public class Player {
	
	protected Board board;
	protected Symbol symbol;
	boolean active;
	
	public Player() {
		symbol = Symbol.VOID;
	}
	
	public Player(Board board){
		this.board = board;
	}
	
	public Player(Board board, Symbol symbol){
		this(board);
		this.symbol = symbol;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}
	
	public Symbol getSymbol() {
		return symbol;
	}
	
	public void setSymbol(Symbol symbol) {
		this.symbol = symbol;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public char move(int x, int y) throws TicTacToeException {
		checkLimits(x, y);
		checkValue(x, y);
		board.setValueAt(x, y, symbol.getSymbol());
		return board.getValueAt(x, y);
	}
	
	private void checkValue(int x, int y) throws TicTacToeException {
		if(' ' != board.getValueAt(x,y))
			throw new TicTacToeException("You can't put value in this position");
	}


	private void checkLimits(int x, int y) throws TicTacToeException {
		board.checkLimits(x, y);
	}

	public boolean winCondition() {
		return board.winCondition(symbol);
	}
	
	public boolean loseCondition(Symbol symbol){
		return board.loseCondition(symbol);
	}
	
	public boolean loseCondition() {
		return loseCondition(symbol);
	}

	public boolean checkDiagonals() {
		return board.checkDiagonals(symbol);
	}

	public boolean checkHorizontal() {
		return board.checkHorizontal(symbol);
	}
	
	public boolean checkVertical() {
		return board.checkVertical(symbol);
	}

	public boolean checkDraw() {
		return board.checkDraw();
	}
	
	public boolean checkColumn(int i) {
		return board.checkColumn(i, getSymbol());
	}

	public boolean checkRow(int i) {
		return board.checkRow(i, getSymbol());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (active != other.active)
			return false;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		if (symbol.getSymbol() != other.symbol.getSymbol())
			return false;
		return true;
	}

};
