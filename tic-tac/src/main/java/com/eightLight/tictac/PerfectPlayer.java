package com.eightLight.tictac;


public class PerfectPlayer extends Player {
	
	public PerfectPlayer() {
		super();
	}
	
	public PerfectPlayer(Board board){
		super(board);
	}
	
	public PerfectPlayer(Board board, Symbol symbol){
		super(board, symbol);
	}
	
	public PerfectPlayer(Player player){
		this(player.board, player.symbol);
	}
	
}
