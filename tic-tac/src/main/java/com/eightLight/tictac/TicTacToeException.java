package com.eightLight.tictac;

public class TicTacToeException extends Exception {

	public TicTacToeException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6690612770812308773L;

}
