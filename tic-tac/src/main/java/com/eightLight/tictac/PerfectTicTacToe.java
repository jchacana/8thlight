package com.eightLight.tictac;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PerfectTicTacToe extends TicTacToe {

	private int[] nextBestPossibleMove;
	
	public PerfectTicTacToe(Board board, Player player1, Player player2 ) {
		super(board, player1, player2);
	}

	@Override
	protected void play(Player player) throws TicTacToeException {
		//FIXME this should call a play method on every type of
		//Player instead of checking instance
		if((player instanceof PerfectPlayer)){
			play((PerfectPlayer)player, board);
			int[] move = nextBestPossibleMove;
			player.move(move[0], move[1]);
			setActiveTurn(player);
			player.active = true;
			if(player == player1) player2.active = false;
			else player1.active = false;
			
		} else {
			super.play(player);
		}
	}

	private int play(Player player, Board board) {
		Board newStateBoard = new Board(board);
		
		List<Integer> scores = new ArrayList<Integer>();
		if(newStateBoard.endState()){
			return score(newStateBoard);
		}
		List<int[]> moves = generateCurrentLevelMoves(newStateBoard);
		Player nextPlayer = next(player);
		for(int[] move: moves){
			newStateBoard.setValueAt(move[0], move[1], player.getSymbol().getSymbol());
			scores.add(play(nextPlayer, newStateBoard));
			newStateBoard = new Board(board);
		}
		if(player == getActiveTurn()){
			int maxScore = Collections.max(scores);
			int maxScoreIndex = scores.indexOf(maxScore);
			setNextBestPossibleMove(moves.get(maxScoreIndex));
			return maxScore;
		} else {
			int minScore = Collections.min(scores);
			int minScoreIndex = scores.indexOf(minScore);
			setNextBestPossibleMove(moves.get(minScoreIndex));
			return minScore;
		}
		
	}
	
	public List<int[]> generateCurrentLevelMoves(Board board) {
		List<int[]> moves = new ArrayList<int[]>();
		for(int i = 0; i < board.getBoardSize(); i++){
			for(int j = 0; j < board.getBoardSize(); j++){
				if(board.getBoard()[i][j] == ' ') 
					moves.add(new int[]{i,j});
			}
		}
		return moves;
	}

	private Player next(Player player) {
		if(player == player1){ 
			return player2;
		} else {
			return player1;
		}
	}

	private int score(Board board) {
		Player active = getActiveTurn();
		if(board.winCondition(active.getSymbol())) return 10;
		else if(board.loseCondition(active.getSymbol())) return -10;
		return 0;
	}

	public int[] getNextBestPossibleMove() {
		return nextBestPossibleMove;
	}

	public void setNextBestPossibleMove(int[] nextBestPossibleMove) {
		this.nextBestPossibleMove = nextBestPossibleMove;
	}
	
	
}
