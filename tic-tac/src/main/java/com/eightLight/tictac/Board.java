package com.eightLight.tictac;

import java.util.Arrays;


public class Board {
	
	private char [][] board = {{' ', ' ', ' '},{' ', ' ', ' '},{' ', ' ', ' '}};
	
	public Board() {
		board = new char[][]{{' ', ' ', ' '},{' ', ' ', ' '},{' ', ' ', ' '}};
	}
	
	public Board(char [][] board){
		this.board = board;
	}
	
	public Board(Board board){
		int length = board.board.length;
		for(int i = 0; i < this.board.length; i++){
			this.board[i] = Arrays.copyOf(board.board[i], length);
		}
	}
	
	public char getValueAt(int x, int y){
		return board[x][y]; 
	}
	
	public void setValueAt(int x, int y, char value){
		board[x][y] = value;
	}
	
	public int getBoardSize(){
		return board.length;
	}
	
	public void checkLimits(int x, int y) throws TicTacToeException{
		if(x < 0 || x >= board.length )
			throw new TicTacToeException("Coord Off Limits");
		if(y < 0 || y >= board[0].length)
			throw new TicTacToeException("Coord Off Limits");
	}
	
	public boolean checkDiagonals(Symbol symbol) {
		return checkPrimaryDiagonal(symbol) || checkSecondaryDiagonal(symbol);
	}
	
	private boolean checkSecondaryDiagonal(Symbol symbol) {
		int boardSize = board.length;
		boolean result = true;
		for(int i = 0; i < boardSize; i++){
			result = result && (board[i][boardSize-1-i] == symbol.getSymbol());
			if(!result) return result;
		}
		return result;
	}

	private boolean checkPrimaryDiagonal(Symbol symbol) {
		int boardSize = board.length;
		boolean result = true;
		for(int i = 0; i < boardSize; i++){
			result = result && (board[i][i] == symbol.getSymbol());
			if(!result) break;
		}
		return result;
	}

	public boolean checkHorizontal(Symbol symbol) {
		boolean result = false;
		for(int i = 0; i < board.length; i++){
			result = result || checkRow(i,symbol);
		}
		return result;
	}
	
	public boolean checkVertical(Symbol symbol) {
		boolean result = false;
		for(int i = 0; i < board.length; i++){
			result = result || checkColumn(i,symbol);
		}
		return result;
	}
	
	public boolean checkColumn(int columnIndex, Symbol symbol){
		boolean result = true;
		for(int i = 0; i < board.length; i++){
			result = result && (board[i][columnIndex] == symbol.getSymbol());
			if(!result) break;
		}
		return result;
	}
	
	public boolean checkRow(int rowIndex, Symbol symbol) {
		boolean result = true;
		for(int i = 0; i < board.length; i++){
			result = result && (board[rowIndex][i] == symbol.getSymbol());
			if(!result) break;
		}
		return result;
	}
	
	public char[][] getBoard() {
		return board;
	}
	
	public void setBoard(char[][] board) {
		this.board = board;
	}

	public boolean checkDraw() {
		return !winCondition() && isFullBoard();
	}
	
	public boolean isFullBoard() {
		for(int i = 0; i < board.length; i++){
			for(int j = 0; j < board[i].length; j++){
				if(board[i][j] == ' ') return false;
			}
		}
		return true;
	}

	public boolean winCondition(Symbol symbol){
		return checkPlayer(symbol);
	}
	
	public boolean winCondition() {
		return checkPlayer(Symbol.X) || checkPlayer(Symbol.O);
	}
	
	public boolean loseCondition(Symbol symbol){
		boolean lose = false;
		switch(symbol){
			case X: 
				lose = winCondition(Symbol.O);
				break;
			case O:
				lose = winCondition(Symbol.X);
				break;
			default:
				break;
				
		}
		return lose;
	}
	
	//FIXME Could this be improved??
	public boolean endState(){
		return winCondition() || isFullBoard();
	}

	private boolean checkPlayer(Symbol symbol) {
		return checkDiagonals(symbol) || checkHorizontal(symbol) || checkVertical(symbol);
	}

	public void resetBoard() {
		board = new char[][]{{' ', ' ', ' '},{' ', ' ', ' '},{' ', ' ', ' '}};
	}
	
	public void printCurrentBoard(){
		System.out.print(this);
	}
	
	@Override
	public String toString() {
		StringBuilder sbr = new StringBuilder();
		sbr.append("-------------\n");
		for(int i = 0; i < board.length; i++){
			for(int j = 0; j < board[i].length; j++){
				sbr.append("| " + board[i][j] + " ");
			}
			sbr.append("|\n");
			sbr.append("-------------\n");
		}
		return sbr.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}

}
