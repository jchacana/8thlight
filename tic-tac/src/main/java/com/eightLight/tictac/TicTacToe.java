package com.eightLight.tictac;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This is the main class
 * I'll implement in a subclass the algorithm for calculating the best
 * move in order to win or draw always.
 
 * @author jchacana
 *
 */
public class TicTacToe {
	 
	protected Board board;
	protected Player player1;
	protected Player player2;
	private Player activeTurn;
	
	public TicTacToe() {
		
	}

	public TicTacToe(Player player1, Player player2){
		this.player1 = player1;
		this.player2 = player2;
	}
	
	public TicTacToe(Board board, Player player1, Player player2) {
		this(player1, player2);
		this.board = board;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}
	
	public Player getPlayer1() {
		return player1;
	}
	
	public Player getPlayer2() {
		return player2;
	}
	
	public Player getActiveTurn() {
		return activeTurn;
	}
	
	public void setActiveTurn(Player activeTurn) {
		this.activeTurn = activeTurn;
	}
	
	public void resetGame(){
		board.resetBoard();
		player1.setBoard(board);
		player2.setBoard(board);
	}
	
	public void gameLoop(){
		int option = 0;
		board.printCurrentBoard();
		do{
			resetGame();
			try {
				option = firstLoop();
			} catch (InputMismatchException e) {
				System.out.println("Please enter valid input");
			}
		} while(option != 3);
		System.exit(0);
	}

	private int firstLoop() {
		int option;
		printMenu();
		option = scanMainInput();
		switch(option){
			case 1: playGame(player1, player2);
					break;
			case 2: playGame(player2, player1);
					break;
			default:
					break;
		}
		return option;
	}

	private void playGame(Player first, Player second) {
		activeTurn = first;
		first.setActive(true);
		second.setActive(false);
		do{
			try {
				play(activeTurn);
				nextTurn(first, second);
			} catch (TicTacToeException e) {
				System.out.println(e.getMessage());
			}
		}while(!board.endState());
		board.printCurrentBoard();
		System.out.println("--- Game Ended ---");
	}

	protected void nextTurn(Player first, Player second) {
		if(activeTurn == first) {
			activeTurn = second;
			second.setActive(true);
			first.setActive(false);
		} else {
			activeTurn = first;
			first.setActive(true);
			second.setActive(false);
		}
	}

	protected void play(Player player) throws TicTacToeException {
		int[] xy;
		board.printCurrentBoard();
		System.out.print("Player " + player.getSymbol().getSymbol() + ": ");
		xy = getNextMove();
		player.move(xy[0], xy[1]);
	}

	protected int[] getNextMove() {
		System.out.print("Enter x and y coord separated by space: ");
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		int y = in.nextInt();
		return new int[]{x, y};
	}

	private int scanMainInput() {
		Scanner in = new Scanner(System.in);
		int option = in.nextInt();
		return option;
	}

	protected int[] scanInput() {
		Scanner in = new Scanner(System.in);
		System.out.print("Please enter x coord: ");
		int x = in.nextInt();
		System.out.print("Please enter y coord: ");
		int y = in.nextInt();
		return new int[]{x,y};
	}

	//TODO This menu should be printed by something else
	//and use the gameLoop method
	public void printMenu() {
		System.out.println("----- Tic Tac Toe -----");
		System.out.println("1.- Starts human");
		System.out.println("2.- Starts computer");
		System.out.println("3.- Exit");
	}
	
	public static void main(String[] args) {
		Player playerHuman, playerComputer;
		Board board = new Board();
		playerHuman = new Player(board, Symbol.X);
		playerComputer = new PerfectPlayer(board, Symbol.O);
		TicTacToe game = new PerfectTicTacToe(board, playerHuman, playerComputer);
		
		game.gameLoop();
	}

}
